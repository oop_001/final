/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ktyp.oppfinal;

/**
 *
 * @author toey
 */
public class Drink {
    private String name;
    private String namedrink;
    private int glass;
    private int price;

    public Drink(String name, String namedrink, int glass, int price) {
        this.name = name;
        this.namedrink = namedrink;
        this.glass = glass;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getNamedrink() {
        return namedrink;
    }

    public int getGlass() {
        return glass;
    }

    public int getPrice() {
        return price;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setNamedrink(String namedrink) {
        this.namedrink = namedrink;
    }

    public void setGlass(int glass) {
        this.glass = glass;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Name: " + name+" "+ namedrink +" ("+ glass  +") "+price + " price/glass"   ;
    }    
}
