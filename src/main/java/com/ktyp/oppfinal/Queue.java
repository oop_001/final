/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ktyp.oppfinal;

/**
 *
 * @author toey
 */
public class Queue {
        private String name ; 
    
     public Queue( String name) {
        this.name= name;
     }
     
      public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
